#include "Voie.h"
#include "EnsembleDeVoies.h"
#include "EnsembleDeVoiesOrientees.h"
#include "Itineraire.h"

/*
 * Identifiants des voies de la gare de Denis
 */
enum {
  /* Voies d'entrée et de sortie de la gare */
  VOIE_LIBRE_0, VOIE_LIBRE_1, VOIE_LIBRE_2, VOIE_LIBRE_3, VOIE_LIBRE_4,
  VOIE_LIBRE_5, VOIE_LIBRE_6, VOIE_LIBRE_7, VOIE_LIBRE_8, VOIE_LIBRE_9,
  /* Voies de liaison */
  VOIE_0,  VOIE_1,  VOIE_2,  VOIE_3,  VOIE_4,  VOIE_5,  VOIE_6,  VOIE_7,  VOIE_8,
  VOIE_9,  VOIE_10, VOIE_11, VOIE_12, VOIE_13, VOIE_14, VOIE_15, VOIE_16, VOIE_17, 
  VOIE_18, VOIE_19, VOIE_20, VOIE_21, VOIE_22, VOIE_23, VOIE_24, VOIE_25, VOIE_26,
  VOIE_27, VOIE_28, VOIE_29, VOIE_30, VOIE_31, VOIE_32, VOIE_33, VOIE_34, VOIE_35,
  VOIE_36,
  /* Voies de garage */
  VOIE_GARAGE_0, VOIE_GARAGE_1, VOIE_GARAGE_2, VOIE_GARAGE_3, VOIE_GARAGE_4,
  VOIE_GARAGE_5, VOIE_GARAGE_6, VOIE_GARAGE_7, VOIE_GARAGE_8, VOIE_GARAGE_9, 
  VOIE_GARAGE_10, 
  /* Aiguillages */
  AIGUILLE_0,  AIGUILLE_1,  AIGUILLE_2, AIGUILLE_3, AIGUILLE_4,  AIGUILLE_5,
  AIGUILLE_6,  AIGUILLE_7,  AIGUILLE_8, AIGUILLE_9, AIGUILLE_10, AIGUILLE_11,
  AIGUILLE_12, AIGUILLE_13, AIGUILLE_14,
  /* Traversées de jonction doubles */
  TJD_0, TJD_1,  TJD_2,  TJD_3,  TJD_4,  TJD_5,  TJD_6,  TJD_7,  TJD_8,
  TJD_9, TJD_10, TJD_11, TJD_12, TJD_13, TJD_14, TJD_15, TJD_16,
  /* Croisements */
  CROISEMENT_0, CROISEMENT_1, CROISEMENT_2,
  /* Valeur spéciale */
  AUCUNE_VOIE
};

#ifdef DEBUG
void afficheNomVoie(byte id, bool aLaLigne)
{
  switch (id) {
    case VOIE_LIBRE_0:   Serial.print(F("VOIE_LIBRE_0"));     break;
    case VOIE_LIBRE_1:   Serial.print(F("VOIE_LIBRE_1"));     break;
    case VOIE_LIBRE_2:   Serial.print(F("VOIE_LIBRE_2"));     break;
    case VOIE_LIBRE_3:   Serial.print(F("VOIE_LIBRE_3"));     break;
    case VOIE_LIBRE_4:   Serial.print(F("VOIE_LIBRE_4"));     break;
    case VOIE_LIBRE_5:   Serial.print(F("VOIE_LIBRE_5"));     break;
    case VOIE_LIBRE_6:   Serial.print(F("VOIE_LIBRE_6"));     break;
    case VOIE_LIBRE_7:   Serial.print(F("VOIE_LIBRE_7"));     break;
    case VOIE_LIBRE_8:   Serial.print(F("VOIE_LIBRE_8"));     break;
    case VOIE_LIBRE_9:   Serial.print(F("VOIE_LIBRE_9"));     break;
    case VOIE_0:         Serial.print(F("VOIE_0"));           break;
    case VOIE_1:         Serial.print(F("VOIE_1"));           break;
    case VOIE_2:         Serial.print(F("VOIE_2"));           break;
    case VOIE_3:         Serial.print(F("VOIE_3"));           break;
    case VOIE_4:         Serial.print(F("VOIE_4"));           break;
    case VOIE_5:         Serial.print(F("VOIE_5"));           break;
    case VOIE_6:         Serial.print(F("VOIE_6"));           break;
    case VOIE_7:         Serial.print(F("VOIE_7"));           break;
    case VOIE_8:         Serial.print(F("VOIE_8"));           break;
    case VOIE_9:         Serial.print(F("VOIE_9"));           break;
    case VOIE_10:        Serial.print(F("VOIE_10"));          break;
    case VOIE_11:        Serial.print(F("VOIE_11"));          break;
    case VOIE_12:        Serial.print(F("VOIE_12"));          break;
    case VOIE_13:        Serial.print(F("VOIE_13"));          break;
    case VOIE_14:        Serial.print(F("VOIE_14"));          break;
    case VOIE_15:        Serial.print(F("VOIE_15"));          break;
    case VOIE_16:        Serial.print(F("VOIE_16"));          break;
    case VOIE_17:        Serial.print(F("VOIE_17"));          break;
    case VOIE_18:        Serial.print(F("VOIE_18"));          break;
    case VOIE_19:        Serial.print(F("VOIE_19"));          break;
    case VOIE_20:        Serial.print(F("VOIE_20"));          break;
    case VOIE_21:        Serial.print(F("VOIE_21"));          break;
    case VOIE_22:        Serial.print(F("VOIE_22"));          break;
    case VOIE_23:        Serial.print(F("VOIE_23"));          break;
    case VOIE_24:        Serial.print(F("VOIE_24"));          break;
    case VOIE_25:        Serial.print(F("VOIE_25"));          break;
    case VOIE_26:        Serial.print(F("VOIE_26"));          break;
    case VOIE_27:        Serial.print(F("VOIE_27"));          break;
    case VOIE_28:        Serial.print(F("VOIE_28"));          break;
    case VOIE_29:        Serial.print(F("VOIE_29"));          break;
    case VOIE_30:        Serial.print(F("VOIE_30"));          break;
    case VOIE_31:        Serial.print(F("VOIE_31"));          break;
    case VOIE_32:        Serial.print(F("VOIE_32"));          break;
    case VOIE_33:        Serial.print(F("VOIE_33"));          break;
    case VOIE_34:        Serial.print(F("VOIE_34"));          break;
    case VOIE_35:        Serial.print(F("VOIE_35"));          break;
    case VOIE_36:        Serial.print(F("VOIE_36"));          break;
    case VOIE_GARAGE_0:  Serial.print(F("VOIE_GARAGE_0"));    break;
    case VOIE_GARAGE_1:  Serial.print(F("VOIE_GARAGE_1"));    break;
    case VOIE_GARAGE_2:  Serial.print(F("VOIE_GARAGE_2"));    break;
    case VOIE_GARAGE_3:  Serial.print(F("VOIE_GARAGE_3"));    break;
    case VOIE_GARAGE_4:  Serial.print(F("VOIE_GARAGE_4"));    break;
    case VOIE_GARAGE_5:  Serial.print(F("VOIE_GARAGE_5"));    break;
    case VOIE_GARAGE_6:  Serial.print(F("VOIE_GARAGE_6"));    break;
    case VOIE_GARAGE_7:  Serial.print(F("VOIE_GARAGE_7"));    break;
    case VOIE_GARAGE_8:  Serial.print(F("VOIE_GARAGE_8"));    break;
    case VOIE_GARAGE_9:  Serial.print(F("VOIE_GARAGE_9"));    break;
    case VOIE_GARAGE_10: Serial.print(F("VOIE_GARAGE_10"));   break;
    case AIGUILLE_0:     Serial.print(F("AIGUILLE_0"));       break;
    case AIGUILLE_1:     Serial.print(F("AIGUILLE_1"));       break;
    case AIGUILLE_2:     Serial.print(F("AIGUILLE_2"));       break;
    case AIGUILLE_3:     Serial.print(F("AIGUILLE_3"));       break;
    case AIGUILLE_4:     Serial.print(F("AIGUILLE_4"));       break;
    case AIGUILLE_5:     Serial.print(F("AIGUILLE_5"));       break;
    case AIGUILLE_6:     Serial.print(F("AIGUILLE_6"));       break;
    case AIGUILLE_7:     Serial.print(F("AIGUILLE_7"));       break;
    case AIGUILLE_8:     Serial.print(F("AIGUILLE_8"));       break;
    case AIGUILLE_9:     Serial.print(F("AIGUILLE_9"));       break;
    case AIGUILLE_10:    Serial.print(F("AIGUILLE_10"));      break;
    case AIGUILLE_11:    Serial.print(F("AIGUILLE_11"));      break;
    case AIGUILLE_12:    Serial.print(F("AIGUILLE_12"));      break;
    case AIGUILLE_13:    Serial.print(F("AIGUILLE_13"));      break;
    case AIGUILLE_14:    Serial.print(F("AIGUILLE_14"));      break;
    case TJD_0:          Serial.print(F("TJD_0"));            break;
    case TJD_1:          Serial.print(F("TJD_1"));            break;
    case TJD_2:          Serial.print(F("TJD_2"));            break;
    case TJD_3:          Serial.print(F("TJD_3"));            break;
    case TJD_4:          Serial.print(F("TJD_4"));            break;
    case TJD_5:          Serial.print(F("TJD_5"));            break;
    case TJD_6:          Serial.print(F("TJD_6"));            break;
    case TJD_7:          Serial.print(F("TJD_7"));            break;
    case TJD_8:          Serial.print(F("TJD_8"));            break;
    case TJD_9:          Serial.print(F("TJD_9"));            break;
    case TJD_10:         Serial.print(F("TJD_10"));           break;
    case TJD_11:         Serial.print(F("TJD_11"));           break;
    case TJD_12:         Serial.print(F("TJD_12"));           break;
    case TJD_13:         Serial.print(F("TJD_13"));           break;
    case TJD_14:         Serial.print(F("TJD_14"));           break;
    case TJD_15:         Serial.print(F("TJD_15"));           break;
    case TJD_16:         Serial.print(F("TJD_16"));           break;
    case CROISEMENT_0:   Serial.print(F("CROISEMENT_0"));     break;
    case CROISEMENT_1:   Serial.print(F("CROISEMENT_1"));     break;
    case CROISEMENT_2:   Serial.print(F("CROISEMENT_2"));     break;
    case AUCUNE_VOIE:    Serial.print(F("** AUCUNE_VOIE**")); break;
    default: Serial.print(F("** Voie inconnue **"));          break;
  }
  if (aLaLigne) Serial.println();
}

void afficheLigne(byte nb)
{
  while(nb--) {
    Serial.print('-');
  }
  Serial.println();
}
#endif

/*
 * Création des objets qui composent la gare
 *
 * Les voies de garage
 */
VoieEnImpasse voieGarage0(VOIE_GARAGE_0);
VoieEnImpasse voieGarage1(VOIE_GARAGE_1);
VoieEnImpasse voieGarage2(VOIE_GARAGE_2);
VoieEnImpasse voieGarage3(VOIE_GARAGE_3);
VoieEnImpasse voieGarage4(VOIE_GARAGE_4);
VoieEnImpasse voieGarage5(VOIE_GARAGE_5);
VoieEnImpasse voieGarage6(VOIE_GARAGE_6);
VoieEnImpasse voieGarage7(VOIE_GARAGE_7);
VoieEnImpasse voieGarage8(VOIE_GARAGE_8);
VoieEnImpasse voieGarage9(VOIE_GARAGE_9);
VoieEnImpasse voieGarage10(VOIE_GARAGE_10);
/*
 * Les voies d'entrée et de sortie
 */
VoieNormale voieLibre0(VOIE_LIBRE_0);
VoieNormale voieLibre1(VOIE_LIBRE_1);
VoieNormale voieLibre2(VOIE_LIBRE_2);
VoieNormale voieLibre3(VOIE_LIBRE_3);
VoieNormale voieLibre4(VOIE_LIBRE_4);
VoieNormale voieLibre5(VOIE_LIBRE_5);
VoieNormale voieLibre6(VOIE_LIBRE_6);
VoieNormale voieLibre7(VOIE_LIBRE_7);
VoieNormale voieLibre8(VOIE_LIBRE_8);
VoieNormale voieLibre9(VOIE_LIBRE_9);
/*
 * Les voies de liaison
 */
VoieNormale voie0(VOIE_0);
VoieNormale voie1(VOIE_1);
VoieNormale voie2(VOIE_2);
VoieNormale voie3(VOIE_3);
VoieNormale voie4(VOIE_4);
VoieNormale voie5(VOIE_5);
VoieNormale voie6(VOIE_6);
VoieNormale voie7(VOIE_7);
VoieNormale voie8(VOIE_8);
VoieNormale voie9(VOIE_9);
VoieNormale voie10(VOIE_10);
VoieNormale voie11(VOIE_11);
VoieNormale voie12(VOIE_12);
VoieNormale voie13(VOIE_13);
VoieNormale voie14(VOIE_14);
VoieNormale voie15(VOIE_15);
VoieNormale voie16(VOIE_16);
VoieNormale voie17(VOIE_17);
VoieNormale voie18(VOIE_18);
VoieNormale voie19(VOIE_19);
VoieNormale voie20(VOIE_20);
VoieNormale voie21(VOIE_21);
VoieNormale voie22(VOIE_22);
VoieNormale voie23(VOIE_23);
VoieNormale voie24(VOIE_24);
VoieNormale voie25(VOIE_25);
VoieNormale voie26(VOIE_26);
VoieNormale voie27(VOIE_27);
VoieNormale voie28(VOIE_28);
VoieNormale voie29(VOIE_29);
VoieNormale voie30(VOIE_30);
VoieNormale voie31(VOIE_31);
VoieNormale voie32(VOIE_32);
VoieNormale voie33(VOIE_33);
VoieNormale voie34(VOIE_34);
VoieNormale voie35(VOIE_35);
VoieNormale voie36(VOIE_36);
/*
 * Aiguillages
 */
Aiguillage aiguille0(AIGUILLE_0);
Aiguillage aiguille1(AIGUILLE_1);
Aiguillage aiguille2(AIGUILLE_2);
Aiguillage aiguille3(AIGUILLE_3);
Aiguillage aiguille4(AIGUILLE_4);
Aiguillage aiguille5(AIGUILLE_5);
Aiguillage aiguille6(AIGUILLE_6);
Aiguillage aiguille7(AIGUILLE_7);
Aiguillage aiguille8(AIGUILLE_8);
Aiguillage aiguille9(AIGUILLE_9);
Aiguillage aiguille10(AIGUILLE_10);
Aiguillage aiguille11(AIGUILLE_11);
Aiguillage aiguille12(AIGUILLE_12);
Aiguillage aiguille13(AIGUILLE_13);
Aiguillage aiguille14(AIGUILLE_14);
/*
 * Croisements
 */
Croisement croisement0(CROISEMENT_0);
Croisement croisement1(CROISEMENT_1);
Croisement croisement2(CROISEMENT_2);
/*
 * TJDs
 */
TraverseeJonctionDouble tjd0(TJD_0);
TraverseeJonctionDouble tjd1(TJD_1);
TraverseeJonctionDouble tjd2(TJD_2);
TraverseeJonctionDouble tjd3(TJD_3);
TraverseeJonctionDouble tjd4(TJD_4);
TraverseeJonctionDouble tjd5(TJD_5);
TraverseeJonctionDouble tjd6(TJD_6);
TraverseeJonctionDouble tjd7(TJD_7);
TraverseeJonctionDouble tjd8(TJD_8);
TraverseeJonctionDouble tjd9(TJD_9);
TraverseeJonctionDouble tjd10(TJD_10);
TraverseeJonctionDouble tjd11(TJD_11);
TraverseeJonctionDouble tjd12(TJD_12);
TraverseeJonctionDouble tjd13(TJD_13);
TraverseeJonctionDouble tjd14(TJD_14);
TraverseeJonctionDouble tjd15(TJD_15);
TraverseeJonctionDouble tjd16(TJD_16);

/*
 * Table des sources
 */
const int nombreDeSources = 9;
const byte sources[nombreDeSources] = {
  VOIE_LIBRE_0, VOIE_LIBRE_1, VOIE_LIBRE_2, VOIE_LIBRE_3, VOIE_LIBRE_4,
  VOIE_LIBRE_5, VOIE_LIBRE_6, VOIE_GARAGE_0, VOIE_GARAGE_1
};

/*
 * Table des destinations
 */
const int nombreDeDestinations = 12;
const byte destinations[nombreDeDestinations] = {
  VOIE_GARAGE_2, VOIE_GARAGE_3, VOIE_GARAGE_4, VOIE_GARAGE_5, VOIE_GARAGE_6,
  VOIE_GARAGE_7, VOIE_GARAGE_8, VOIE_GARAGE_9, VOIE_GARAGE_10, VOIE_LIBRE_7, 
  VOIE_LIBRE_8, VOIE_LIBRE_9 
};

unsigned long nombreItineraires = 0;
unsigned long nombreDeVides = 0;

void testItineraire(byte source, byte destination)
{
  EnsembleItineraires iti;
  Voie::voirParIdentifiant(source).cheminsVers(destination, DIRECTION_AVANT, iti);
  unsigned long combien = iti.nombre();
  nombreItineraires += combien;
  if (combien == 0) nombreDeVides++;

//  Serial.println("################");
//  Serial.print("De ");
//  afficheNomVoie(source);
//  Serial.print(" a ");
//  afficheNomVoie(destination, true);
//  iti.print();
}

void setup() {
  Voie::finaliser();
  
  Serial.begin(115200);
  Serial.println("Gestionnaire reseau");

  /*
   * Connexion des voies de bas en haut et de gauche à droite
   */
  voieLibre0.connecteSortantAVoie(aiguille0, ENTRANT);
  aiguille0.connecteSortantDevieAVoie(voie0, ENTRANT);
  aiguille0.connecteSortantDroitAVoie(voie1, ENTRANT);
  voie0.connecteSortantAVoie(tjd0, ENTRANT_BIS);
  voie1.connecteSortantAVoie(tjd8, ENTRANT_BIS);
  
  voieLibre1.connecteSortantAVoie(tjd0, ENTRANT);
  tjd0.connecteSortantAVoie(voie3, ENTRANT);
  tjd0.connecteSortantBisAVoie(voie2, ENTRANT);
  voie3.connecteSortantAVoie(tjd8, ENTRANT);
  tjd8.connecteSortantAVoie(voie22, ENTRANT);
  tjd8.connecteSortantBisAVoie(voie21, ENTRANT);
  voie22.connecteSortantAVoie(aiguille12, SORTANT_DEVIE);
  aiguille12.connecteEntrantAVoie(voieGarage2, SORTANT);
  voie32.connecteSortantAVoie(aiguille12, SORTANT_DROIT);

  voieLibre2.connecteSortantAVoie(tjd1, ENTRANT);
  voie2.connecteSortantAVoie(tjd1, ENTRANT_BIS);
  tjd1.connecteSortantAVoie(voie8, ENTRANT);
  tjd1.connecteSortantBisAVoie(voie6, ENTRANT);
  voie8.connecteSortantAVoie(tjd12, ENTRANT);
  voie21.connecteSortantAVoie(tjd12, ENTRANT_BIS);
  tjd12.connecteSortantAVoie(voie27, ENTRANT);
  tjd12.connecteSortantBisAVoie(croisement2, ENTRANT_BIS);
  voie27.connecteSortantAVoie(tjd15, ENTRANT_BIS);
  croisement2.connecteSortantAVoie(tjd15, ENTRANT);
  tjd15.connecteSortantBisAVoie(voie31, ENTRANT);
  tjd15.connecteSortantAVoie(voie32, ENTRANT);
  voie31.connecteSortantAVoie(aiguille11, ENTRANT);
  aiguille11.connecteSortantDevieAVoie(voieGarage3, SORTANT);
  aiguille11.connecteSortantDroitAVoie(voieGarage4, SORTANT);

  voieLibre3.connecteSortantAVoie(tjd2, ENTRANT);
  voie6.connecteSortantAVoie(tjd2, ENTRANT_BIS);
  tjd2.connecteSortantAVoie(voie12, ENTRANT);
  tjd2.connecteSortantBisAVoie(voie11, ENTRANT);
  voie12.connecteSortantAVoie(tjd11, ENTRANT_BIS);
  voie20.connecteSortantAVoie(tjd11, ENTRANT);
  tjd11.connecteSortantBisAVoie(voie26, ENTRANT);
  tjd11.connecteSortantAVoie(croisement2, ENTRANT);
  voie26.connecteSortantAVoie(tjd14, ENTRANT);
  croisement2.connecteSortantBisAVoie(tjd14, ENTRANT_BIS);
  tjd14.connecteSortantAVoie(voie30, ENTRANT);
  tjd14.connecteSortantBisAVoie(voie29, ENTRANT);
  voie30.connecteSortantAVoie(aiguille10, ENTRANT);
  aiguille10.connecteSortantDroitAVoie(voieGarage5, SORTANT);
  aiguille10.connecteSortantDevieAVoie(voie34, ENTRANT);
  voie34.connecteSortantAVoie(aiguille14, SORTANT_DROIT);
  voie33.connecteSortantAVoie(aiguille14, SORTANT_DEVIE);
  aiguille14.connecteEntrantAVoie(voieGarage6, SORTANT);
  
  voieLibre4.connecteSortantAVoie(tjd4, ENTRANT);
  voie11.connecteSortantAVoie(tjd4, ENTRANT_BIS);
  tjd4.connecteSortantAVoie(voie16, ENTRANT);
  tjd4.connecteSortantBisAVoie(croisement0, ENTRANT_BIS);
  voie16.connecteSortantAVoie(tjd7, ENTRANT_BIS);
  croisement0.connecteSortantAVoie(tjd7, ENTRANT);
  tjd7.connecteSortantAVoie(voie20, ENTRANT);
  tjd7.connecteSortantBisAVoie(voie19, ENTRANT);
  voie19.connecteSortantAVoie(aiguille8, SORTANT_DROIT);
  voie25.connecteSortantAVoie(aiguille8, SORTANT_DEVIE);
  aiguille8.connecteEntrantAVoie(voie28, ENTRANT);
  voie28.connecteSortantAVoie(tjd16, ENTRANT);
  voie29.connecteSortantAVoie(tjd16, ENTRANT_BIS);
  tjd16.connecteSortantAVoie(voie33, ENTRANT);
  tjd16.connecteSortantBisAVoie(voieGarage7, SORTANT);

  voieLibre5.connecteSortantAVoie(tjd3, ENTRANT_BIS);
  voie10.connecteSortantAVoie(tjd3, ENTRANT);
  tjd3.connecteSortantAVoie(croisement0, ENTRANT);
  tjd3.connecteSortantBisAVoie(voie15, ENTRANT);
  voie15.connecteSortantAVoie(tjd6, ENTRANT);
  croisement0.connecteSortantBisAVoie(tjd6, ENTRANT_BIS);
  tjd6.connecteSortantAVoie(voie18, ENTRANT);
  tjd6.connecteSortantBisAVoie(croisement1, ENTRANT_BIS);
  voie18.connecteSortantAVoie(tjd10, ENTRANT_BIS);
  croisement1.connecteSortantAVoie(tjd10, ENTRANT);
  tjd10.connecteSortantAVoie(voie25, ENTRANT);
  tjd10.connecteSortantBisAVoie(voieGarage8, SORTANT);

  voieLibre6.connecteSortantAVoie(aiguille3, SORTANT_DROIT);
  voie5.connecteSortantAVoie(aiguille3, SORTANT_DEVIE);
  aiguille3.connecteEntrantAVoie(aiguille4, ENTRANT);
  aiguille4.connecteSortantDroitAVoie(voie9, ENTRANT);
  aiguille4.connecteSortantDevieAVoie(voie10, ENTRANT);
  voie9.connecteSortantAVoie(tjd5, ENTRANT_BIS);
  voie14.connecteSortantAVoie(tjd5, ENTRANT);
  tjd5.connecteSortantAVoie(croisement1, ENTRANT);
  tjd5.connecteSortantBisAVoie(voie17, ENTRANT);
  voie17.connecteSortantAVoie(tjd9, ENTRANT);
  croisement1.connecteSortantBisAVoie(tjd9, ENTRANT_BIS);
  tjd9.connecteSortantAVoie(voie24, ENTRANT);
  tjd9.connecteSortantBisAVoie(voie23, ENTRANT);
  voie24.connecteSortantAVoie(aiguille9, SORTANT_DROIT);
  voie36.connecteSortantAVoie(aiguille9, SORTANT_DEVIE);
  aiguille9.connecteEntrantAVoie(voieGarage9, SORTANT);

  voieGarage0.connecteAVoie(aiguille1, ENTRANT);
  aiguille1.connecteSortantDroitAVoie(voie5, ENTRANT);  
  aiguille1.connecteSortantDevieAVoie(voie4, ENTRANT);

  voieGarage1.connecteAVoie(aiguille2, SORTANT_DEVIE);
  voie4.connecteSortantAVoie(aiguille2, SORTANT_DROIT);
  aiguille2.connecteEntrantAVoie(voie7, ENTRANT);
  voie7.connecteSortantAVoie(aiguille5, ENTRANT);
  aiguille5.connecteSortantDevieAVoie(voieLibre9, ENTRANT);
  aiguille5.connecteSortantDroitAVoie(aiguille6, ENTRANT);  
  aiguille6.connecteSortantDroitAVoie(voieLibre8, ENTRANT);  
  aiguille6.connecteSortantDevieAVoie(aiguille7, ENTRANT);
  aiguille7.connecteSortantDroitAVoie(voie14, ENTRANT);
  aiguille7.connecteSortantDevieAVoie(voie13, ENTRANT);
  voie13.connecteSortantAVoie(tjd13, ENTRANT);
  voie23.connecteSortantAVoie(tjd13, ENTRANT_BIS);
  tjd13.connecteSortantAVoie(voie36, ENTRANT);
  tjd13.connecteSortantBisAVoie(voie35, ENTRANT);
  voie35.connecteSortantAVoie(aiguille13, ENTRANT);
  aiguille13.connecteSortantDroitAVoie(voieGarage10, SORTANT);
  aiguille13.connecteSortantDevieAVoie(voieLibre7, ENTRANT);

  voieLibre7.connecteSortantAVoie(voieLibre0, ENTRANT);
#ifdef DEBUG
  Serial.print(F("Nombre de voies : "));
  Serial.println((unsigned long)Voie::nombre());

  Serial.println(F("**** Test des ensembles ****"));

  /* Test des ensembles */
  EnsembleDeVoies ensemble;
  ensemble.ajouteVoie(VOIE_GARAGE_4);
  ensemble.ajouteVoie(VOIE_LIBRE_6);
  ensemble.ajouteVoie(aiguille13);
  ensemble.println();
  ensemble.enleveVoie(voieLibre6);
  ensemble.println();

  /* Test des ensembles orientés */
  Serial.println(F("**** Test des ensembles orientes ****"));
  EnsembleDeVoiesOrientees ens2;
  ens2.ajouteVoie(VOIE_GARAGE_4, DIRECTION_AVANT);
  ens2.printlnraw();
  ens2.println();
  ens2.ajouteVoie(VOIE_GARAGE_4, DIRECTION_ARRIERE);
  ens2.printlnraw();
  ens2.println();
  ens2.ajouteVoie(VOIE_LIBRE_6, DIRECTION_ARRIERE);
  ens2.printlnraw();
  ens2.println();
  ens2.ajouteVoie(VOIE_GARAGE_2, DIRECTION_AVANT);
  ens2.printlnraw();
  ens2.println();
  ens2.enleveVoie(VOIE_GARAGE_4, DIRECTION_AVANT);
  ens2.printlnraw();
  ens2.println();
  ens2.enleveVoie(VOIE_GARAGE_4, DIRECTION_ARRIERE);
  ens2.printlnraw();
  ens2.println();
  ens2.enleveVoie(VOIE_LIBRE_6, DIRECTION_ARRIERE);
  ens2.printlnraw();
  ens2.println();
  ens2.enleveVoie(VOIE_GARAGE_2, DIRECTION_AVANT);
  ens2.printlnraw();
  ens2.println();
  
  /* Test d'itineraires */
  EnsembleItineraires iti3;
  voieLibre6.cheminsVers(VOIE_GARAGE_4, DIRECTION_AVANT, iti3);
  iti3.print();

  EnsembleItineraires iti4;
  voieLibre0.cheminsVers(VOIE_GARAGE_7, DIRECTION_AVANT, iti4);
  iti4.print();

  EnsembleItineraires iti5;
  voieLibre1.cheminsVers(VOIE_GARAGE_2, DIRECTION_AVANT, iti5);
  iti5.print();

  EnsembleItineraires iti6;
  voie26.cheminsVers(VOIE_GARAGE_6, DIRECTION_AVANT, iti6);
  iti6.print();

  testItineraire(VOIE_26, VOIE_GARAGE_6);

  Serial.println("**** Test de tous les itineraires ****");
  unsigned long topDepart = millis();
  for (int src = 0; src < nombreDeSources; src++) {
    for (int dst = 0; dst < nombreDeDestinations; dst++) {
      testItineraire(sources[src], destinations[dst]);
    }
  }
  unsigned long topArrivee = millis();
  Serial.print("Duree : ");
  Serial.println(topArrivee - topDepart);
  Serial.print("Nombre d'itineraires : ");
  Serial.println(nombreItineraires);
  Serial.print("Nombre d'impossibilites : ");
  Serial.println(nombreDeVides);
  Serial.println("Fin du test de tous les itineraires");
#endif
}

void loop() {
  // put your main code here, to run repeatedly:

}
