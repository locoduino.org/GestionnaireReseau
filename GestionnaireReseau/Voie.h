/*
 * Classes de description des éléments de voie
 */
#ifndef __VOIE_H__
#define __VOIE_H__
 
#include "Debug.h"

/*
 * Sens de parcours
 */
enum { DIRECTION_AVANT = 0, DIRECTION_ARRIERE = 1, AUCUNE_DIRECTION = 2 };

/*
 * Connecteurs d'un élément de voie
 */
enum { ENTRANT, SORTANT, SORTANT_DEVIE, SORTANT_DROIT, ENTRANT_BIS, SORTANT_BIS };


#ifdef DEBUG
/*
 * Affichage du nom d'un connecteur (debug)
 */
void afficheNomConnecteur(byte connecteur, bool aLaLigne = false);

/*
 * Affichage du sens
 */
void afficheSens(byte dir);
#endif

class EnsembleItineraires;
class EnsembleDeVoiesOrientees;

/*
 * Classe de base d'un élément de voie
 */
class Voie
{
  private:
    byte mIdentifiant;    /* Identifiant de l'élément                                     */
    byte mDirection;      /* Direction normale de parcours                                */
    static byte sNombre;  /* Compteur d'élements de voie                                  */
    static Voie **sVoies; /* Table de pointeurs vers les voies indexé par identifiant     */
    static word sTailleTableau; /* taille du tableau de voies                             */
    
  protected:
    void fixeDirection(byte dir); /* fixe la direction de parcours */
    
  public:
    Voie(byte identifiant);
    byte idVoie() { return mIdentifiant; }
    byte direction() { return mDirection; }
    static const byte nombre() { return sNombre; }
    static const byte taille() {
      return ((sNombre >> 3) + ((sNombre & 7) != 0));
    }
    static void finaliser(); /* A appeler au debut de setup pour retailler le tableau des voies */
    static Voie &voirParIdentifiant(byte identifiant) { return *sVoies[identifiant]; }
    virtual void connecteDeVoie(Voie *voie, byte connecteur) = 0;
    
    virtual bool tousLesCheminsVers(
      byte identifiant, 
      byte dir, 
      EnsembleItineraires &chemins, 
      Voie *venantDe,
      EnsembleDeVoiesOrientees &marquage) = 0;
      
    bool cheminsVers(Voie &voie, byte dir, EnsembleItineraires &chemins);
    bool cheminsVers(byte identifiant, byte dir, EnsembleItineraires &chemins);
};

/*
 * Classe pour une voie en impasse
 */
class VoieEnImpasse : public Voie
{
  private:
    Voie *mVoie;    /* Connecteur SORTANT */
    word mLongueur; /* Longueur de la voie en impasse */
    
  public:
    VoieEnImpasse(byte identifiant, word longueur = 0) :
      Voie(identifiant) { mVoie = NULL; mLongueur = longueur; }
    virtual bool tousLesCheminsVers(
      byte identifiant, 
      byte dir, 
      EnsembleItineraires &chemins, 
      Voie *venantDe,
      EnsembleDeVoiesOrientees &marquage);
    void connecteAVoie(Voie &voie, byte connecteur);
    virtual void connecteDeVoie(Voie *voie, byte connecteur);
};

/*
 * Classe pour une voie de liaison normale
 */
class VoieNormale : public Voie
{
  private:
    Voie *mVoieSortant;
    Voie *mVoieEntrant;
    word mLongueur;     /* Longueur de la voie normale */

  public:
    VoieNormale(byte identifiant, word longueur = 0) :
      Voie(identifiant) { mVoieSortant = mVoieEntrant = NULL; mLongueur = longueur; }
    virtual bool tousLesCheminsVers(
      byte identifiant, 
      byte dir, 
      EnsembleItineraires &chemins, 
      Voie *venantDe,
      EnsembleDeVoiesOrientees &marquage);
    void connecteSortantAVoie(Voie &voie, byte connecteur);
    void connecteEntrantAVoie(Voie &voie, byte connecteur);
    virtual void connecteDeVoie(Voie *voie, byte connecteur);
};

/*
 * Classe de base pour les objets possedant plus de 2 connecteurs
 * et qui donc doivent apparaitre dans les itineraires
 */
class VoieMulti : public Voie
{
  private:
    static byte sNombre;

  public:
    VoieMulti(byte identifiant) : Voie(identifiant) {}
};

/*
 * Classe pour les Aiguillages
 */
class Aiguillage : public VoieMulti
{
  private:
    Voie *mVoieEntrant;
    Voie *mVoieSortantDroit;
    Voie *mVoieSortantDevie;
    EnsembleItineraires *mItinerairePartiel;
    word mLongueurDroit; /* longueur de la voie droite de l'aiguillage */
    word mLongueurDevie; /* longueur de la voie deviee de l'aiguillage */

  public:
    Aiguillage(byte identifiant, word longueurDroit = 0, word longueurDevie = 0) :
      VoieMulti(identifiant) {
        mVoieEntrant = mVoieSortantDroit = mVoieSortantDevie = NULL;
        mItinerairePartiel = NULL;
        mLongueurDroit = longueurDroit;
        mLongueurDevie = longueurDevie;
      }
    virtual bool tousLesCheminsVers(
      byte identifiant, 
      byte dir, 
      EnsembleItineraires &chemins, 
      Voie *venantDe,
      EnsembleDeVoiesOrientees &marquage);
    void connecteSortantDroitAVoie(Voie &voie, byte connecteur);
    void connecteSortantDevieAVoie(Voie &voie, byte connecteur);
    void connecteEntrantAVoie(Voie &voie, byte connecteur);
    virtual void connecteDeVoie(Voie *voie, byte connecteur);
};

/*
 * Classe pour les croisements
 */
class Croisement : public VoieMulti
{
  protected:
    Voie *mVoieEntrant;
    Voie *mVoieSortant;
    Voie *mVoieEntrantBis;
    Voie *mVoieSortantBis;
    word mLongueur;
    word mLongueurBis;
    
  public:
    Croisement(byte identifiant, word longueur = 0, word longueurBis = 0) :
      VoieMulti(identifiant) {
        mVoieEntrant = mVoieSortant = mVoieEntrantBis = mVoieSortantBis = NULL;
        mLongueur = longueur;
        mLongueurBis = longueurBis;
      }
    virtual bool tousLesCheminsVers(
      byte identifiant, 
      byte dir, 
      EnsembleItineraires &chemins, 
      Voie *venantDe,
      EnsembleDeVoiesOrientees &marquage);
    void connecteSortantAVoie(Voie &voie, byte connecteur);
    void connecteSortantBisAVoie(Voie &voie, byte connecteur);
    void connecteEntrantAVoie(Voie &voie, byte connecteur);
    void connecteEntrantBisAVoie(Voie &voie, byte connecteur);
    virtual void connecteDeVoie(Voie *voie, byte connecteur);
};

/*
 * Classe pour les TJD : une variante des croisements
 */
class TraverseeJonctionDouble : public Croisement
{
  private:
    EnsembleItineraires *mItinerairePartiel[2]; /* un pour chaque direction */
  
  protected:
    virtual bool tousLesCheminsVers(
      byte identifiant, 
      byte dir, 
      EnsembleItineraires &chemins, 
      Voie *venantDe,
      EnsembleDeVoiesOrientees &marquage);
  
  public:
    TraverseeJonctionDouble(byte identifiant, word longueur = 0, word longueurBis = 0) :
      Croisement(identifiant, longueur, longueurBis) {
        mItinerairePartiel[0] = mItinerairePartiel[1] = NULL;
      }
};

#endif

