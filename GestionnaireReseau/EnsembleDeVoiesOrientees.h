/*
 *  Ensemble de voies orientées.
 *  L'ensemble est un vecteur de bits indexé par l'identifiant
 *  des voies * 2 + l'sens : 0 sens avant, 1 sens arriere.
 *  Chaque bit indique l'appartenance d'une voie à l'ensemble
 *  - 1 la Voie appartient à l'ensemble
 *  - 0 elle n'appartient pas à l'ensemble.
 */
#ifndef __ENSEMBLEDEVOIESORIENTEES_H__
#define __ENSEMBLEDEVOIESORIENTEES_H__

#include "HardwareSerial.h"
#include "Voie.h"

class EnsembleDeVoiesOrientees
{
  private:
    void alloue();
  
  protected:
    byte *mEnsemble;

  public:
    EnsembleDeVoiesOrientees(); /* Construit un ensemble vide */
    EnsembleDeVoiesOrientees(const EnsembleDeVoiesOrientees &ens);
    ~EnsembleDeVoiesOrientees();
    void vide();
    void ajouteVoie(byte identifiant, byte sens);
    void ajouteVoie(Voie &voie, byte sens) {
     ajouteVoie(voie.idVoie(), sens);
    }
    void ajouteVoie(Voie *voie, byte sens) {
      ajouteVoie(voie->idVoie(), sens);
    }
    void enleveVoie(byte identifiant, byte sens);
    void enleveVoie(Voie &voie, byte sens) {
      enleveVoie(voie.idVoie(), sens);
    }
    void enleveVoie(Voie *voie, byte sens) {
      enleveVoie(voie->idVoie(), sens);
    }
    bool contientVoie(byte identifiant, byte sens);
    bool estVide();
    bool contientVoie(Voie *voie, byte sens) {
      return contientVoie(voie->idVoie(), sens);
    }
    bool contientVoie(Voie &voie, byte sens) {
      return contientVoie(voie.idVoie(), sens);
    }
    EnsembleDeVoiesOrientees &operator=(const EnsembleDeVoiesOrientees &ens);
    bool operator==(EnsembleDeVoiesOrientees &operande);
#ifdef DEBUG
    void print();
    void println();
    void printraw();
    void printlnraw();
#endif
};

#endif

