/*
 *  Ensemble de voie. L'ensemble est un vecteur de bits indexé par l'identifiant
 *  des voies. Chaque bit indique l'appartenance d'une voie à l'ensemble
 *  - 1 la Voie appartient à l'ensemble
 *  - 0 elle n'appartient pas à l'ensemble.
 */
#include "EnsembleDeVoies.h"

void EnsembleDeVoies::alloue()
{
  mEnsemble = new byte[Voie::taille()];
}

EnsembleDeVoies::EnsembleDeVoies()
{
  alloue();
  vide();
}

EnsembleDeVoies::EnsembleDeVoies(const EnsembleDeVoies &ens)
{
//  Serial.println("  Constructeur par recopie");
//  delay(1000);  
  alloue();
  operator=(ens);
}

EnsembleDeVoies::~EnsembleDeVoies()
{
//  Serial.println("  destruction d'un ensemble de voies");
//  delay(1000);
  delete mEnsemble;
//  Serial.println("  destruction terminee");
//  delay(1000);
}

void EnsembleDeVoies::vide()
{
  for (byte i = 0; i < Voie::taille(); i++) {
    mEnsemble[i] = 0;
  }
}

bool EnsembleDeVoies::estVide()
{
  for (byte i = 0; i < Voie::taille(); i++) {
    if (mEnsemble[i] != 0) return false;
  }
  return true;
}

void EnsembleDeVoies::ajouteVoie(byte identifiant)
{
  mEnsemble[identifiant >> 3] |= 1 << (identifiant & 7);
}

void EnsembleDeVoies::enleveVoie(byte identifiant)
{
  mEnsemble[identifiant >> 3] &= ~(1 << (identifiant & 7));
}

bool EnsembleDeVoies::contientVoie(byte identifiant)
{
  return (mEnsemble[identifiant >> 3] & 1 << (identifiant & 7)) != 0;
}

EnsembleDeVoies &EnsembleDeVoies::operator=(const EnsembleDeVoies &ens)
{
  for (byte i = 0; i < Voie::taille(); i++) {
    mEnsemble[i] = ens.mEnsemble[i];
  }
  return *this;
}

bool EnsembleDeVoies::operator==(EnsembleDeVoies &operande)
{
  bool resultat = true;
  for (byte i = 0; i < Voie::taille(); i++) {
    resultat = resultat && (mEnsemble[i] == operande.mEnsemble[i]);
  }
  return resultat; 
}


#ifdef DEBUG
void EnsembleDeVoies::print()
{
  bool ensembleVide = true;
  for (byte i = 0; i < Voie::taille(); i++) {
    byte sousVecteur = mEnsemble[i];
    for (byte j = 0; j < 8; j++) {
      byte identifiant = (i << 3) + j;
      if (sousVecteur & (1 << j)) {
        ensembleVide = false;
        afficheNomVoie(identifiant);
        Serial.print(' ');
      }
    }
  }
  if (ensembleVide) {
    Serial.print("*vide*");
  }
}

void EnsembleDeVoies::println()
{
  print();
  Serial.println();
}
#endif

