/*
 * Itineraire : un ensemble de voie avec un pointeur pour constituer
 * des listes d'itinéraires
 */
#ifndef __ITINERAIRE_H__
#define __ITINERAIRE_H__

#include "EnsembleDeVoies.h"

class EnsembleItineraires;

class Itineraire : public EnsembleDeVoies
{
  private:
    Itineraire *mSuivant; /* pour chainer           */
    friend class EnsembleItineraires;

  public:
    Itineraire() : EnsembleDeVoies() { mSuivant = NULL; }
    Itineraire(const Itineraire &iti) : EnsembleDeVoies(iti) { mSuivant = NULL; }
    bool estCompatibleAvec(const Itineraire &iti);
};

/*
 * Ensemble d'itineraires allant d'une voie de départ à une voie
 * d'arrivée en suivant une direction
 */
class EnsembleItineraires
{
  private:
    Itineraire *mTeteDeListe;
    byte mDepart;
    byte mArrivee;
    byte mDirection;

    void efface();
    void copie(EnsembleItineraires &itis);

  public:
    EnsembleItineraires() { mTeteDeListe = new Itineraire(); }
    EnsembleItineraires(EnsembleItineraires &ens);
    ~EnsembleItineraires();
    void ajouteVoie(byte identifiant);
    void ajouteVoie(Voie *voie) { ajouteVoie(voie->idVoie()); }
    bool contientItineraire(Itineraire &iti);
    EnsembleItineraires &operator+=(EnsembleItineraires &itis);
    EnsembleItineraires &operator=(EnsembleItineraires &itis);
    unsigned long nombre();
#ifdef DEBUG
    void print();
    void println();
#endif
};

#endif

