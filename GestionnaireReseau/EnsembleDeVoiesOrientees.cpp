/*
 *  Ensemble de voies orientées.
 *  L'ensemble est un vecteur de bits indexé par l'identifiant
 *  des voies * 2 + l'orientation : 0 sens avant, 1 sens arriere.
 *  Chaque bit indique l'appartenance d'une voie à l'ensemble
 *  - 1 la Voie appartient à l'ensemble
 *  - 0 elle n'appartient pas à l'ensemble.
 */
#include "EnsembleDeVoiesOrientees.h"
#include "Voie.h"

void EnsembleDeVoiesOrientees::alloue()
{
  mEnsemble = new byte[2 * Voie::taille()];
#ifdef TRACE2
  Serial.print(F("*** EnsembleDeVoiesOrientees : allocation de "));
  Serial.print(2 * Voie::taille());
  Serial.println(F(" octets"));
#endif
}

EnsembleDeVoiesOrientees::EnsembleDeVoiesOrientees()
{
  alloue();
  vide();
}

EnsembleDeVoiesOrientees::EnsembleDeVoiesOrientees(
  const EnsembleDeVoiesOrientees &ens)
{
//  Serial.println("  Constructeur par recopie");
//  delay(1000);  
  alloue();
  operator=(ens);
}

EnsembleDeVoiesOrientees::~EnsembleDeVoiesOrientees()
{
//  Serial.println("  destruction d'un ensemble de voies");
//  delay(1000);
  delete mEnsemble;
//  Serial.println("  destruction terminee");
//  delay(1000);
}

void EnsembleDeVoiesOrientees::vide()
{
  for (byte i = 0; i < (2 * Voie::taille()); i++) {
    mEnsemble[i] = 0;
  }
}

bool EnsembleDeVoiesOrientees::estVide()
{
  for (byte i = 0; i < 2 * Voie::taille(); i++) {
    if (mEnsemble[i] != 0) return false;
  }
  return true;
}

void EnsembleDeVoiesOrientees::ajouteVoie(byte identifiant, byte sens)
{
  mEnsemble[(identifiant >> 2)] |=
    1 << (((identifiant & 3) << 1) + sens );
#ifdef TRACE2
  Serial.print(F("*** EnsembleDeVoiesOrientees : Ajout octet "));
  Serial.print(identifiant >> 2);
  Serial.print(F(" bit "));
  Serial.println(((identifiant & 3) << 1) + sens);
#endif
}

void EnsembleDeVoiesOrientees::enleveVoie(byte identifiant, byte sens)
{
  mEnsemble[(identifiant >> 2)] &=
    ~(1 << (((identifiant & 3) << 1) + sens ));
}

bool EnsembleDeVoiesOrientees::contientVoie(byte identifiant, byte sens)
{
  return (mEnsemble[(identifiant >> 2)] &
    1 << (((identifiant & 3) << 1) + sens )) != 0;
}

EnsembleDeVoiesOrientees &EnsembleDeVoiesOrientees::operator=(
  const EnsembleDeVoiesOrientees &ens)
{
  for (byte i = 0; i < 2 * Voie::taille(); i++) {
    mEnsemble[i] = ens.mEnsemble[i];
  }
  return *this;
}

bool EnsembleDeVoiesOrientees::operator==(EnsembleDeVoiesOrientees &operande)
{
  bool resultat = true;
  for (byte i = 0; i < 2 * Voie::taille(); i++) {
    resultat = resultat && (mEnsemble[i] == operande.mEnsemble[i]);
  }
  return resultat; 
}


#ifdef DEBUG
void EnsembleDeVoiesOrientees::print()
{
  bool ensembleVide = true;
  for (byte i = 0; i < 2 * Voie::taille(); i++) {
    byte sousVecteur = mEnsemble[i];
    for (byte j = 0; j < 4; j++) {
      byte identifiant = (i << 2) + j;
      if (sousVecteur & (1 << (2 * j))) {
        ensembleVide = false;
        afficheNomVoie(identifiant);
        Serial.print('/');
        afficheSens(DIRECTION_AVANT);
        Serial.print(' ');
      }
      if (sousVecteur & (1 << ((2 * j) +1))) {
        ensembleVide = false;
        afficheNomVoie(identifiant);
        Serial.print('/');
        afficheSens(DIRECTION_ARRIERE);
        Serial.print(' ');
      }
    }
  }
  if (ensembleVide) {
    Serial.print("*vide*");
  }
}

void EnsembleDeVoiesOrientees::println()
{
  print();
  Serial.println();
}

void EnsembleDeVoiesOrientees::printraw()
{
  for (byte i = 0; i < 2 * Voie::taille(); i++) {
    Serial.print((unsigned long)mEnsemble[i], HEX);
    Serial.print(' ');
  }
}

void EnsembleDeVoiesOrientees::printlnraw()
{
  printraw();
  Serial.println();
}
#endif

