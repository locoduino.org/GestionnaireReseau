/*
 *  Ensemble de voie. L'ensemble est un vecteur de bits indexé par l'identifiant
 *  des voies. Chaque bit indique l'appartenance d'une voie à l'ensemble
 *  - 1 la Voie appartient à l'ensemble
 *  - 0 elle n'appartient pas à l'ensemble.
 */
#ifndef __ENSEMBLEDEVOIES_H__
#define __ENSEMBLEDEVOIES_H__

#include "HardwareSerial.h"
#include "Voie.h"

class EnsembleDeVoies
{
  private:
    void alloue();
  
  protected:
    byte *mEnsemble;

  public:
    EnsembleDeVoies(); /* Construit un ensemble vide */
    EnsembleDeVoies(const EnsembleDeVoies &ens);
    ~EnsembleDeVoies();
    void vide();
    void ajouteVoie(byte identifiant);
    void ajouteVoie(Voie &voie) { ajouteVoie(voie.idVoie()); }
    void ajouteVoie(Voie *voie) { ajouteVoie(voie->idVoie()); }
    void enleveVoie(byte identifiant);
    void enleveVoie(Voie &voie) { enleveVoie(voie.idVoie()); }
    void enleveVoie(Voie *voie) { enleveVoie(voie->idVoie()); }
    bool contientVoie(byte identifiant);
    bool estVide();
    bool contientVoie(Voie *voie) { return contientVoie(voie->idVoie()); }
    bool contientVoie(Voie &voie) { return contientVoie(voie.idVoie()); }
    EnsembleDeVoies &operator=(const EnsembleDeVoies &ens);
    bool operator==(EnsembleDeVoies &operande);
    
#ifdef DEBUG
    void print();
    void println();
#endif
};

#endif

