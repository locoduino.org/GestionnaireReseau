#include "Voie.h"
#include "Itineraire.h"
#include "EnsembleDeVoiesOrientees.h"

#ifdef DEBUG
/*
 * Messages d'erreur
 */
const char msgErreurConnecteur[] PROGMEM =
  "Connecteur incorrect : ";
const char msgErreurConnecteurDejaUtilise[] PROGMEM =
  "Connecteur deja utilise : ";

void afficheNomConnecteur(byte connecteur, bool aLaLigne)
{
  switch (connecteur) {
    case ENTRANT:       Serial.print(F("ENTRANT"));       break;
    case SORTANT:       Serial.print(F("SORTANT"));       break;
    case SORTANT_DEVIE: Serial.print(F("SORTANT_DEVIE")); break;
    case SORTANT_DROIT: Serial.print(F("SORTANT_DROIT")); break;
    case ENTRANT_BIS:   Serial.print(F("ENTRANT_BIS"));   break;
    case SORTANT_BIS:   Serial.print(F("SORTANT_BIS"));   break;
    default: Serial.print(F("** Connecteur inconnu **")); break;
  }
  if (aLaLigne) Serial.println();
}

void afficheVoieConnecteur(byte idVoie, byte connecteur)
{
    afficheNomVoie(idVoie);
    Serial.print('(');
    afficheNomConnecteur(connecteur);
    Serial.println(')');
}

int profondeur = 0;

void afficheVoieProfondeur(byte identifiant)
{
#ifdef TRACE
  int i = profondeur << 2;
  while (i--) Serial.print(' ');
  afficheNomVoie(identifiant);
  Serial.println();
#endif
}

void afficheSens(byte sens)
{
  switch (sens) {
    case DIRECTION_AVANT:   Serial.print("AV"); break;
    case DIRECTION_ARRIERE: Serial.print("AR"); break;
    case AUCUNE_DIRECTION:  Serial.print("*");  break;
  }
}
#endif

/*
 * Classe Voie : classe abstraite de base pour les éléments de voie
 */
byte Voie::sNombre = 0;
Voie **Voie::sVoies = NULL;
word Voie::sTailleTableau = 256;

Voie::Voie(byte identifiant)
{
  mIdentifiant = identifiant;
  mDirection = AUCUNE_DIRECTION;
  sNombre++;
  /* Si sVoies n'est pas alloue, alloue arbitrairement 256 emplacements */
  if (sVoies == NULL) {
    sVoies = (Voie **)malloc(sTailleTableau * sizeof(Voie **));
  }
  sVoies[identifiant] = this;
}

void Voie::finaliser()
{
  sTailleTableau = sNombre;
  sVoies = (Voie **)realloc(sVoies, sTailleTableau * sizeof(Voie **));
}

void Voie::fixeDirection(byte dir)
{
  if (mDirection == AUCUNE_DIRECTION || mDirection == dir) mDirection = dir;
#ifdef DEBUG
  else {
    Serial.print(F("Direction deja fixee : "));
    afficheNomVoie(idVoie(), true);
  }
#endif
}

bool Voie::cheminsVers(Voie &voie, byte dir, EnsembleItineraires &chemins)
{
  profondeur = 0;
  EnsembleDeVoiesOrientees marquage;
  return tousLesCheminsVers(voie.idVoie(), dir, chemins, NULL, marquage);
}

bool Voie::cheminsVers(byte identifiant, byte dir, EnsembleItineraires &chemins)
{
  profondeur = 0;
  EnsembleDeVoiesOrientees marquage;
  return tousLesCheminsVers(identifiant, dir, chemins, NULL, marquage);
}

/*
 * Classe pour une voie en impasse.
 */
void VoieEnImpasse::connecteAVoie(Voie &voie, byte connecteur)
{
  if (mVoie == NULL) {
    mVoie = &voie;
    mVoie->connecteDeVoie(this, connecteur);
    fixeDirection(DIRECTION_AVANT);
  }
#ifdef DEBUG
  else {
    Serial.print(msgErreurConnecteurDejaUtilise);
    afficheVoieConnecteur(idVoie(),SORTANT);
  }
#endif
}

void VoieEnImpasse::connecteDeVoie(Voie *voie, byte connecteur)
{
  if (connecteur == SORTANT) {
    if (mVoie == NULL) {
      mVoie = voie;
      fixeDirection(DIRECTION_ARRIERE);
    }
#ifdef DEBUG
    else {
      Serial.print(msgErreurConnecteurDejaUtilise);
      afficheVoieConnecteur(idVoie(),connecteur);
    }
#endif
  }
#ifdef DEBUG
  else {
    Serial.print(msgErreurConnecteur);
    afficheVoieConnecteur(idVoie(),connecteur);
  }
#endif
}

bool VoieEnImpasse::tousLesCheminsVers(
  byte identifiant,                   /* identifiant de la voie cible */
  byte dir,                           /* direction de parcours        */
  EnsembleItineraires &chemins,       /* chemins trouvés              */
  Voie *venantDe,                     /* de quelle voie on vient      */
  EnsembleDeVoiesOrientees &marquage  /* pour marquer les voies visitées */ )
{
  afficheVoieProfondeur(idVoie());
  profondeur++;

  if (marquage.contientVoie(this, dir)) {
    /* voie deja visitee, on retourne qu'il n'y a pas de chemin */
    return false;
  }
  else {
    /* Marque la voie comme visitee */
    marquage.ajouteVoie(this, dir);
    if (identifiant == idVoie()) {
      chemins.ajouteVoie(this);
      profondeur--;
      return true;
    }
    else {
      if (mVoie != NULL && direction() == dir) {
        if (mVoie->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
          chemins.ajouteVoie(this);
          profondeur--;
          return true;
        }
      }
    }
    profondeur--;
    return false;
  }
}

/*
 * Classe pour une voie de liaison normale
 */
void VoieNormale::connecteSortantAVoie(Voie &voie, byte connecteur)
{
  if (mVoieSortant == NULL) {
    mVoieSortant = &voie;
    mVoieSortant->connecteDeVoie(this, connecteur);
    fixeDirection(DIRECTION_AVANT);
  }
#ifdef DEBUG
  else {
    Serial.print(F("connecteSortantAVoie/"));
    Serial.print(msgErreurConnecteurDejaUtilise);
    afficheVoieConnecteur(idVoie(),SORTANT);    
  }
#endif
}

void VoieNormale::connecteEntrantAVoie(Voie &voie, byte connecteur)
{
  if (mVoieEntrant == NULL) {
    mVoieEntrant = &voie;
    mVoieEntrant->connecteDeVoie(this, connecteur);
    fixeDirection(DIRECTION_ARRIERE);
  }
#ifdef DEBUG
  else {
    Serial.print(F("connecteEntrantAVoie/"));
    Serial.print(msgErreurConnecteurDejaUtilise);
    afficheVoieConnecteur(idVoie(),ENTRANT);    
  }
#endif
}

void VoieNormale::connecteDeVoie(Voie *voie, byte connecteur)
{
  if (connecteur == SORTANT) {
    if (mVoieSortant == NULL) {
      mVoieSortant = voie;
      fixeDirection(DIRECTION_ARRIERE);
    }
#ifdef DEBUG
    else {
      Serial.print(F("connecteDeVoie/"));
      Serial.print(msgErreurConnecteurDejaUtilise);
      afficheVoieConnecteur(idVoie(),connecteur);
    }
#endif
  }
  else if (connecteur == ENTRANT) {
    if (mVoieEntrant == NULL) {
      mVoieEntrant = voie;
      fixeDirection(DIRECTION_AVANT);
    }    
#ifdef DEBUG
    else {
      Serial.print(F("connecteDeVoie/"));
      Serial.print(msgErreurConnecteurDejaUtilise);
      afficheVoieConnecteur(idVoie(),connecteur);
    }
#endif
  }
#ifdef DEBUG
  else {
    Serial.print(msgErreurConnecteur);
    afficheVoieConnecteur(idVoie(),connecteur);
  }
#endif
}

bool VoieNormale::tousLesCheminsVers(
  byte identifiant,                   /* identifiant de la voie cible */
  byte dir,                           /* direction de parcours        */
  EnsembleItineraires &chemins,       /* chemins trouvés              */
  Voie *venantDe,                     /* de quelle voie on vient      */
  EnsembleDeVoiesOrientees &marquage  /* pour marquer les voies visitées */ )
{
  afficheVoieProfondeur(idVoie());
  profondeur++;
  
  if (marquage.contientVoie(this, dir)) {
    /* voie deja visitee, on retourne qu'il n'y a pas de chemin */
    return false;
  }
  else {
    /* Marque la voie comme visitee */
    marquage.ajouteVoie(this, dir);
    if (idVoie() == identifiant) {
      chemins.ajouteVoie(this);
      profondeur--;
      return true;
    }
    else {
      if (direction() == dir) {
        if (mVoieSortant != NULL) {
          if (mVoieSortant->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
            chemins.ajouteVoie(this);
            profondeur--;
            return true;
          }
          else {
            profondeur--;
            return false;
          }
        }
        else {
          profondeur--;
          return false;
        }
      }
      else {
        if (mVoieEntrant != NULL) {
          if (mVoieEntrant->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
            chemins.ajouteVoie(this);
            profondeur--;
            return true;
          }
          else {
            profondeur--;
            return false;
          }
        }
        else {
          profondeur--;
          return false;
        }
      }
    }
  }
}

/*
 * Classe de base pour les objets possedant plus de 2 connecteurs
 * et qui donc doivent apparaitre dans les itineraires
 */
byte VoieMulti::sNombre = 0;

/*
 * Classe pour les Aiguillages
 */
void Aiguillage::connecteSortantDroitAVoie(Voie &voie, byte connecteur)
{
  if (mVoieSortantDroit == NULL) {
    mVoieSortantDroit = &voie;
    mVoieSortantDroit->connecteDeVoie(this, connecteur);
    fixeDirection(DIRECTION_AVANT);
  }
#ifdef DEBUG
  else {
    Serial.print(msgErreurConnecteurDejaUtilise);
    afficheVoieConnecteur(idVoie(),SORTANT_DROIT);    
  }
#endif
}

void Aiguillage::connecteSortantDevieAVoie(Voie &voie, byte connecteur)
{
  if (mVoieSortantDevie == NULL) {
    mVoieSortantDevie = &voie;
    mVoieSortantDevie->connecteDeVoie(this, connecteur);
    fixeDirection(DIRECTION_AVANT);
  }
#ifdef DEBUG
  else {
    Serial.print(msgErreurConnecteurDejaUtilise);
    afficheVoieConnecteur(idVoie(),SORTANT_DEVIE);    
  }
#endif
}

void Aiguillage::connecteEntrantAVoie(Voie &voie, byte connecteur)
{
  if (mVoieEntrant == NULL) {
    mVoieEntrant = &voie;
    mVoieEntrant->connecteDeVoie(this, connecteur);
    fixeDirection(DIRECTION_ARRIERE);
  }
#ifdef DEBUG
  else {
    Serial.print(msgErreurConnecteurDejaUtilise);
    afficheVoieConnecteur(idVoie(),ENTRANT);    
  }
#endif
  
}

void Aiguillage::connecteDeVoie(Voie *voie, byte connecteur)
{
  if (connecteur == SORTANT_DROIT) {
    if (mVoieSortantDroit == NULL) {
      mVoieSortantDroit = voie;
      fixeDirection(DIRECTION_ARRIERE);
    }
#ifdef DEBUG
    else {
      Serial.print(msgErreurConnecteurDejaUtilise);
      afficheVoieConnecteur(idVoie(),connecteur);
    }
#endif
  }
  else if (connecteur == SORTANT_DEVIE) {
    if (mVoieSortantDevie == NULL) {
      mVoieSortantDevie = voie;
      fixeDirection(DIRECTION_ARRIERE);
    }    
#ifdef DEBUG
    else {
      Serial.print(msgErreurConnecteurDejaUtilise);
      afficheVoieConnecteur(idVoie(),connecteur);
    }
#endif
  }
  else if (connecteur == ENTRANT) {
    if (mVoieEntrant == NULL) {
      mVoieEntrant = voie;
      fixeDirection(DIRECTION_AVANT);
    }    
#ifdef DEBUG
    else {
      Serial.print(msgErreurConnecteurDejaUtilise);
      afficheVoieConnecteur(idVoie(),connecteur);
    }
#endif
  }
#ifdef DEBUG
  else {
    Serial.print(msgErreurConnecteur);
    afficheVoieConnecteur(idVoie(),connecteur);
  }
#endif
}

bool Aiguillage::tousLesCheminsVers(
  byte identifiant,                   /* identifiant de la voie cible */
  byte dir,                           /* direction de parcours        */
  EnsembleItineraires &chemins,       /* chemins trouvés              */
  Voie *venantDe,                     /* de quelle voie on vient      */
  EnsembleDeVoiesOrientees &marquage  /* pour marquer les voies visitées */ )
{
  afficheVoieProfondeur(idVoie());
  profondeur++;
  
  if (marquage.contientVoie(this, dir)) {
    /* 
     * voie deja visitee. 
     * 1) Un itineraire partiel a ete laisse. un chemin existe vers la cible
     *    et il a deja ete explore
     * 2) Pas d'itineraire partiel laissee, pas d'itineraire vers la cible
     */
    profondeur--;
    if (mItinerairePartiel != NULL) {
#ifdef TRACE
      Serial.println(F("$$$$ Chemin partiel"));
      mItinerairePartiel->println();
#endif
      chemins = *mItinerairePartiel;
      return true;
    }
    /* on retourne qu'il n'y a pas de chemin */
    else return false;
  }
  else {
    /* Marque la voie comme visitee */
    marquage.ajouteVoie(this, dir);
    /* 
     * supprime le chemin partiel laisse par une exploitation precedente
     * Si on est dans la diction talon -> pointe
     */
    if (direction() != dir && mItinerairePartiel != NULL) {
      delete mItinerairePartiel;
      mItinerairePartiel = NULL;
    }


    if (idVoie() == identifiant) {
      chemins.ajouteVoie(this);
      profondeur--;
      return true;
    }
    else {
      if (direction() == dir) { /* parcours de pointe vers talon */
        /* Duplique le chemin actuel */
        EnsembleItineraires cheminsDevies(chemins);
        
        if (mVoieSortantDroit != NULL) {
          if (mVoieSortantDroit->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
            /* Chemin cote sortant droit */
            if (mVoieSortantDevie != NULL) {
              /* Examen cote sortant devie */
              if (mVoieSortantDevie->tousLesCheminsVers(identifiant, dir, cheminsDevies, this, marquage)) {
                /* Les deux branches conduisent à la cible on merge */
                chemins += cheminsDevies;
              }
            }
            chemins.ajouteVoie(this);
            profondeur--;
            return true;
          }
          else {
            /* Pas de chemin cote sortant droit */
            if (mVoieSortantDevie != NULL) {
              if (mVoieSortantDevie->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
                /* Chemin cote sortant devie */
                chemins.ajouteVoie(this);
                profondeur--;
                return true;
              }
            }
          }
        }
        profondeur--;
        return false;
      }
      else { /* Parcours du talon vers la pointe */
        if (mVoieEntrant != NULL) {
          if (mVoieEntrant->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
            chemins.ajouteVoie(this);
            /* Laisse un itineraire partiel */
            mItinerairePartiel = new EnsembleItineraires(chemins);
            if (venantDe == mVoieSortantDroit) {
  //            Serial.println(F(": droit"));
            }
            else {
  //            Serial.println(F(": devie"));
            }
            profondeur--;
            return true;
          }
        }
        profondeur--;
        return false;
      }
    }
  }
}

/*
 * Classe pour les croisements
 */
void Croisement::connecteSortantAVoie(Voie &voie, byte connecteur)
{
  if (mVoieSortant == NULL) {
    mVoieSortant = &voie;
    mVoieSortant->connecteDeVoie(this, connecteur);
    fixeDirection(DIRECTION_AVANT);
  }
#ifdef DEBUG
  else {
    Serial.print(msgErreurConnecteurDejaUtilise);
    afficheVoieConnecteur(idVoie(),SORTANT);    
  }
#endif  
}

void Croisement::connecteSortantBisAVoie(Voie &voie, byte connecteur)
{
  if (mVoieSortantBis == NULL) {
    mVoieSortantBis = &voie;
    mVoieSortantBis->connecteDeVoie(this, connecteur);
    fixeDirection(DIRECTION_AVANT);
  }
#ifdef DEBUG
  else {
    Serial.print(msgErreurConnecteurDejaUtilise);
    afficheVoieConnecteur(idVoie(),SORTANT_BIS);    
  }
#endif  
}

void Croisement::connecteEntrantAVoie(Voie &voie, byte connecteur)
{
  if (mVoieEntrant == NULL) {
    mVoieEntrant = &voie;
    mVoieEntrant->connecteDeVoie(this, connecteur);
    fixeDirection(DIRECTION_ARRIERE);
  }
#ifdef DEBUG
  else {
    Serial.print(msgErreurConnecteurDejaUtilise);
    afficheVoieConnecteur(idVoie(),ENTRANT);    
  }
#endif  
}

void Croisement::connecteEntrantBisAVoie(Voie &voie, byte connecteur)
{
  if (mVoieEntrantBis == NULL) {
    mVoieEntrantBis = &voie;
    mVoieEntrantBis->connecteDeVoie(this, connecteur);
    fixeDirection(DIRECTION_ARRIERE);
  }
#ifdef DEBUG
  else {
    Serial.print(msgErreurConnecteurDejaUtilise);
    afficheVoieConnecteur(idVoie(),ENTRANT_BIS);    
  }
#endif  
}

void Croisement::connecteDeVoie(Voie *voie, byte connecteur)
{
  if (connecteur == SORTANT) {
    if (mVoieSortant == NULL) {
      mVoieSortant = voie;
      fixeDirection(DIRECTION_ARRIERE);
    }
#ifdef DEBUG
    else {
      Serial.print(msgErreurConnecteurDejaUtilise);
      afficheVoieConnecteur(idVoie(),connecteur);
    }
#endif
  }
  else if (connecteur == SORTANT_BIS) {
    if (mVoieSortantBis == NULL) {
      mVoieSortantBis = voie;
      fixeDirection(DIRECTION_ARRIERE);
    }    
#ifdef DEBUG
    else {
      Serial.print(msgErreurConnecteurDejaUtilise);
      afficheVoieConnecteur(idVoie(),connecteur);
    }
#endif
  }
  else if (connecteur == ENTRANT) {
    if (mVoieEntrant == NULL) {
      mVoieEntrant = voie;
      fixeDirection(DIRECTION_AVANT);
    }    
#ifdef DEBUG
    else {
      Serial.print(msgErreurConnecteurDejaUtilise);
      afficheVoieConnecteur(idVoie(),connecteur);
    }
#endif
  }
  else if (connecteur == ENTRANT_BIS) {
    if (mVoieEntrantBis == NULL) {
      mVoieEntrantBis = voie;
      fixeDirection(DIRECTION_AVANT);
    }    
#ifdef DEBUG
    else {
      Serial.print(msgErreurConnecteurDejaUtilise);
      afficheVoieConnecteur(idVoie(),connecteur);
    }
#endif
  }
#ifdef DEBUG
  else {
    Serial.print(msgErreurConnecteur);
    afficheVoieConnecteur(idVoie(),connecteur);
  }
#endif
}

bool Croisement::tousLesCheminsVers(
  byte identifiant,
  byte dir,
  EnsembleItineraires &chemins,
  Voie *venantDe,
  EnsembleDeVoiesOrientees &marquage)
{
  afficheVoieProfondeur(idVoie());
  profondeur++;
  
  if (idVoie() == identifiant) {
    chemins.ajouteVoie(this);
    profondeur--;
    return true;
  }
  else {
    if (direction() == dir) {
      if (venantDe == mVoieEntrant && mVoieSortant != NULL) {
        if (mVoieSortant->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
          chemins.ajouteVoie(this);
          profondeur--;
          return true;
        }
        else {
          profondeur--;
          return false;
        }
      }
      else if (venantDe == mVoieEntrantBis && mVoieSortantBis != NULL) {
        if (mVoieSortantBis->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
          chemins.ajouteVoie(this);
          profondeur--;
          return true;
        }
        else {
          profondeur--;
          return false;
        }
      }
      else {
        profondeur--;
        return false;
      }
    }
    else {
      /* direction() != dir */
      if (venantDe == mVoieSortant && mVoieEntrant != NULL) {
        if (mVoieEntrant->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
          chemins.ajouteVoie(this);
          profondeur--;
          return true;
        }
        else {
          profondeur--;
          return false;
        }
      }
      else if (venantDe == mVoieSortantBis && mVoieEntrantBis != NULL) {
        if (mVoieEntrantBis->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
          chemins.ajouteVoie(this);
          profondeur--;
          return true;
        }
        else {
          profondeur--;
          return false;
        }
      }
      else {
        profondeur--;
        return false;
      }
    }
  }
}

/*
 * Classe pour les TJD : une variante des croisements 
 */ 
bool TraverseeJonctionDouble::tousLesCheminsVers(
  byte identifiant,                   /* identifiant de la voie cible */
  byte dir,                           /* direction de parcours        */
  EnsembleItineraires &chemins,       /* chemins trouvés              */
  Voie *venantDe,                     /* de quelle voie on vient      */
  EnsembleDeVoiesOrientees &marquage  /* pour marquer les voies visitées */ )
{
  afficheVoieProfondeur(idVoie());
  profondeur++;
  
  if (marquage.contientVoie(this, dir)) {
    /* 
     * voie deja visitee. 
     * 1) Un itineraire partiel a ete laisse. un chemin existe vers la cible
     *    et il a deja ete explore
     * 2) Pas d'itineraire partiel laissee, pas d'itineraire vers la cible
     */
    profondeur--;
    if (mItinerairePartiel[dir] != NULL) {
#ifdef TRACE
      Serial.println(F("$$$$ Chemin partiel"));
      mItinerairePartiel[dir]->println();
#endif
      chemins = *mItinerairePartiel[dir];
      return true;
    }
    /* on retourne qu'il n'y a pas de chemin */
    else return false;
  }
  else {
    /* Marque la voie comme visitee */
    marquage.ajouteVoie(this, dir);
    /* supprime le chemin partiel laisse par une exploitation precedente */
    if (mItinerairePartiel[dir] != NULL) {
      delete mItinerairePartiel[dir];
      mItinerairePartiel[dir] = NULL;
    }

    if (idVoie() == identifiant) {
      chemins.ajouteVoie(this);
      profondeur--;
      return true;
    }
    else {
      /* Duplique le chemin actuel */
      EnsembleItineraires cheminsBis(chemins);
        
      if (direction() == dir) {
        if (venantDe == mVoieEntrant) {
          if (mVoieSortant != NULL &&
              mVoieSortant->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
            /* Le chemin mene a la cible cote sortant */
            if (mVoieSortantBis != NULL &&
                mVoieSortantBis->tousLesCheminsVers(identifiant, dir, cheminsBis, this, marquage)) {
              /* Le chemin mene egalement a la cible cote sortant bis, on rassemble */
              chemins += cheminsBis;
            }
            chemins.ajouteVoie(this);
            /* Laisse le chemin partiel */
            mItinerairePartiel[dir] = new EnsembleItineraires(chemins);
            profondeur--;
            return true;          
          }
          /* Le chemin ne mene pas a la cible cote sortant */
          else if (mVoieSortantBis != NULL &&
                   mVoieSortantBis->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
            /* Le chemin mene a la sible cote sortant bis */
            chemins.ajouteVoie(this);
            /* Laisse le chemin partiel */
            mItinerairePartiel[dir] = new EnsembleItineraires(chemins);
            profondeur--;
            return true;
          }
          profondeur--;
          return false;
        }
        else if (venantDe == mVoieEntrantBis) {
          if (mVoieSortant != NULL &&
              mVoieSortant->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
            /* Le chemin mene a la cible cote sortant bis */
            if (mVoieSortantBis != NULL &&
                mVoieSortantBis->tousLesCheminsVers(identifiant, dir, cheminsBis, this, marquage)) {
              /* Le chemin mene egalement a la cible cote sortant bis, on rassemble */
              chemins += cheminsBis;
            }
            chemins.ajouteVoie(this);
            /* Laisse le chemin partiel */
            mItinerairePartiel[dir] = new EnsembleItineraires(chemins);
            profondeur--;
            return true;
          }
          /* Le chemin ne mene pas a la cible cote sortant */
          else if (mVoieSortantBis != NULL &&
                   mVoieSortantBis->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
            /* Le chemin mene a la sible cote sortant bis */
            chemins.ajouteVoie(this);
            /* Laisse le chemin partiel */
            mItinerairePartiel[dir] = new EnsembleItineraires(chemins);
            profondeur--;
            return true;
          }
        }
        profondeur--;
        return false;
      }
      else {
        /* direction() != dir */
        if (venantDe == mVoieSortant) {
          if (mVoieEntrant != NULL &&
              mVoieEntrant->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
            /* Le chemin mene a la cible cote entrant */
            if (mVoieEntrantBis != NULL &&
                mVoieEntrantBis->tousLesCheminsVers(identifiant, dir, cheminsBis, this, marquage)) {
              /* Il mene egalement a la cible cote entrant bis, on rassemble */
              chemins += cheminsBis;
            }
            chemins.ajouteVoie(this);
            /* Laisse le chemin partiel */
            mItinerairePartiel[dir] = new EnsembleItineraires(chemins);
            profondeur--;
            return true;
          }
          /* Le chemin ne mene pas a la cible cote entrant */
          else if (mVoieEntrantBis != NULL &&
                   mVoieEntrantBis->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
            chemins.ajouteVoie(this);
            /* Laisse le chemin partiel */
            mItinerairePartiel[dir] = new EnsembleItineraires(chemins);
            profondeur--;
            return true;
          }
          profondeur--;
          return false;
        }
        else if (venantDe == mVoieSortantBis) {
          if (mVoieEntrant != NULL &&
              mVoieEntrant->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
            /* Le chemin mene a la cible cote entrant */
            if (mVoieEntrantBis != NULL &&
                mVoieEntrant->tousLesCheminsVers(identifiant, dir, cheminsBis, this, marquage)) {
              /* Il mene egalement a la cible cote entrant bis, on rassemble */
              chemins += cheminsBis;
            }
            chemins.ajouteVoie(this);
            /* Laisse le chemin partiel */
            mItinerairePartiel[dir] = new EnsembleItineraires(chemins);
            profondeur--;
            return true;
          }
          /* Le chemin ne mene pas a la cible cote entrant */
          else if (mVoieEntrantBis != NULL &&
                   mVoieEntrantBis->tousLesCheminsVers(identifiant, dir, chemins, this, marquage)) {
            chemins.ajouteVoie(this);
            /* Laisse le chemin partiel */
            mItinerairePartiel[dir] = new EnsembleItineraires(chemins);
            profondeur--;
            return true;
          }
        }
        profondeur--;
        return false;
      }
    }
  }
}


