/*
 * Itineraire : un ensemble de voie avec un pointeur pour constituer
 * des listes d'itinéraires
 */
#include "Itineraire.h"

bool Itineraire::estCompatibleAvec(const Itineraire &iti)
{
  byte resultat = 0;
  for (byte i = 0; i < Voie::taille(); i++) {
    resultat |= mEnsemble[i] & iti.mEnsemble[i];
  }
  return (resultat == 0);
}


void EnsembleItineraires::efface()
{
  Itineraire *iti = mTeteDeListe;
  while (iti != NULL) {
    Itineraire *courant = iti;
    iti = iti->mSuivant;
    delete courant;
  }
  mTeteDeListe = NULL;
}

void EnsembleItineraires::copie(EnsembleItineraires &ens)
{
  mTeteDeListe = NULL;
  Itineraire *iti = ens.mTeteDeListe;
  while (iti != NULL) {
    Itineraire *copie = new Itineraire(*iti);
    copie->mSuivant = mTeteDeListe;
    mTeteDeListe = copie;
    iti = iti->mSuivant;
  }  
}

/*
 * Constructeur par recopie
 */
EnsembleItineraires::EnsembleItineraires(EnsembleItineraires &ens)
{
  copie(ens);
}

/*
 * Ajoute une voie à tous les itinéraires contenus dans l'ensemble
 */
EnsembleItineraires::~EnsembleItineraires()
{
  efface();
}

void EnsembleItineraires::ajouteVoie(byte identifiant)
{
#ifdef TRACE
  if (this == NULL) Serial.println(F("Oups!"));
  this->println();
  Serial.print("Ajout de ");
  afficheNomVoie(identifiant, true);
#endif
  Itineraire *iti = mTeteDeListe;
  while (iti != NULL) {
    iti->ajouteVoie(identifiant);
    iti = iti->mSuivant;
  }
}

bool EnsembleItineraires::contientItineraire(Itineraire &iti)
{
  Itineraire *courant = mTeteDeListe;
  while (courant != NULL) {
    if (*courant == iti) {
      return true;
    }
    courant = courant->mSuivant;
  }
  return false;
}

/*
 * Fait l'union de deux ensembles d'itineraires
 */
EnsembleItineraires &EnsembleItineraires::operator+=(EnsembleItineraires &itis)
{
  Itineraire *iti = itis.mTeteDeListe;
  while (iti != NULL) {
    if (! contientItineraire(*iti)) {
      Itineraire *copie = new Itineraire(*iti);
      copie->mSuivant = mTeteDeListe;
      mTeteDeListe = copie;
    }
    iti = iti->mSuivant;
  }
  return *this;
}

/*
 * Copie d'un ensemble d'itineraires
 */
EnsembleItineraires &EnsembleItineraires::operator=(EnsembleItineraires &itis)
{
  /* Si non vide, detruit les itineraires present */
  efface();
  /* Copie */
  copie(itis);
  return *this;
}

unsigned long EnsembleItineraires::nombre()
{
  unsigned long compteur = 0;
  Itineraire *courant = mTeteDeListe;
  while (courant != NULL) {
    if (! courant->estVide()) compteur++;
    courant = courant->mSuivant;
  }
  return compteur;
}

#ifdef DEBUG
void EnsembleItineraires::print()
{
  Itineraire *iti = mTeteDeListe;
  Serial.println("================");
  if (iti == NULL) {
    Serial.println("* Aucun itineraire *");
  }
  while (iti != NULL) {
    iti->println();
    iti = iti->mSuivant;
  }
  Serial.println("================");
}

void EnsembleItineraires::println()
{
  print();
  Serial.println();
}
#endif


