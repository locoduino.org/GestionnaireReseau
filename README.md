# Gestionnaire de réseau

Cette bibliothèque est en cours de développement. C'est pour l'instant une preuve de concept qui n'est pas utilisable pour piloter un réseau.

## Élements de voie

Chaque élément de voie est identifié par un identifiant unique. Afin d'assurer que les identifiants sont uniques et qu'il n'y a pas trou dans la numérotation, il est grandement préférable d'utiliser un ```enum``` anonyme pour les déclarer. 

## Classes disponibles

Pour le moment, les fonctions suivantes sont disponible :

* description du réseau sous forme de graphe d'objets
* recherche d'itinéraires dans le réseau

### Description du réseau

Le principe est de déclarer chaque élément de voie, voie de garage, voie normale, aiguillage, traversée de jonction doucle puis de les connecter. Chaque type d'élément va avoir 1, 2, 3 ou 4 connecteurs nommés comme présenté ci-dessous. Un élément de voie a également un sens. Le sens privilégié est le *sens normal* mais une voie peut être connectée en sens inverse.

#### Voie en impasse

![Voie en impasse](http://www.locoduino.org/pic/GareDenis/voiegarage.png)

**VoieEnImpasse** permet de déclarer une voie en impasse. Sont constructeur prend en argument l'identifiant de la voie. Une voie en impasse ne possède qu'un seul connecteur : *sortant*.

#### Voie Normale

![Voie normale](http://www.locoduino.org/pic/GareDenis/voienormale.png)

**VoieNormale** permet de déclarer une voir normale avec un connecteur *entrant* et un connecteur *sortant*.

#### Aiguillage

![Aiguillage](http://www.locoduino.org/pic/GareDenis/aiguillage.png)

**Aiguillage** permet de déclarer un aiguillage avec un connecteurs *entrant* et deux connecteurs *sortant_droit* et *sortant_devie*.

#### Croisement

![Croisement](http://www.locoduino.org/pic/GareDenis/croisementtjd.png)

**Croisement** permet de déclarer un croisement avec deux connecteurs *entrant* et *entrant_bis* et deux connecteurs *sortant* et *sortant_bis*. *entrant* est raccordé à *sortant* et *entrant_bis* est raccordé à *sortant_bis*.

#### Traversee de jonction double

![Traversee de jonction double](http://www.locoduino.org/pic/GareDenis/croisementtjd.png)

**TraverseeDeJonctionDouble** est très semblable à **Croisement** et possède les mêmes connecteurs. La différence réside dans la manière dont un itinéraire est recherché au travers d'une traversée de jonction double.