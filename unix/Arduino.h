#ifndef ARDUINO_H
#define ARDUINO_H

#include <stdint.h>
#include <stdlib.h>

#define F(str) str
#define PROGMEM

typedef uint8_t  byte;
typedef uint16_t word;

#define true 1
#define false 0

extern unsigned long millis();

#endif